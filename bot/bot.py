import logging
import psycopg2

from aiogram import Bot, Dispatcher, executor, types

API_TOKEN = '5646433057:AAFXHsKVsYqRPFxsK-Rb2qDskQxnMDuayUs'

# Configure logging
logging.basicConfig(level=logging.INFO)

# Initialize bot and dispatcher
bot = Bot(token=API_TOKEN)
dp = Dispatcher(bot)


@dp.message_handler(commands=['start', 'help'])
async def send_welcome(message: types.Message):
    try:
        logging.info(f'({message.from_user.id}) {message.from_user.full_name}: {message.text}')
        await message.reply("Привіт!\nЯ інфобот 3 команди!\n")
    except:
        logging.error(f'({message.from_user.id}) {message.from_user.full_name}: fail {message.text}')


@dp.message_handler(commands=['members'])
async def send_members(message: types.Message):
    try:
        logging.info(f'({message.from_user.id}) {message.from_user.full_name}: {message.text}')
        await message.reply("Телесненко Ірина\nДмитро Луцак\nКравчук Владислав")
    except:
        logging.error(f'({message.from_user.id}) {message.from_user.full_name}: fail {message.text}')
    
    
@dp.message_handler(commands=['photo'])
async def send_photo(message: types.Message):
    try:
        logging.info(f'({message.from_user.id}) {message.from_user.full_name}: {message.text}')
        with open('photos/command3.webp', 'rb') as photo:
            await message.reply_photo(photo, caption='Команда №3😺')
    except:
        logging.error(f'({message.from_user.id}) {message.from_user.full_name}: fail {message.text}')
        

@dp.message_handler(commands=['db'])
async def message_from_db(message: types.Message):
    try:
        logging.info(f'({message.from_user.id}) {message.from_user.full_name}: {message.text}')
        conn = psycopg2.connect(host="db", port = 5432, user="postgres", password="postgres")
        cur = conn.cursor()
        cur.execute("""SELECT * FROM test""")
        query_results = cur.fetchone()
        text = ""
        for item in query_results:
            text = text + item
        await message.reply(f"{text}")
    except:
        logging.error(f'({message.from_user.id}) {message.from_user.full_name}: fail {message.text}')
        
        
@dp.message_handler(commands=['test'])
async def test(message: types.Message):
    logging.warning(f'({message.from_user.id}) {message.from_user.full_name}: Test Warning log')
    await message.reply(f"Test Warning log")
        

if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)